<?php
/**
 * @file
 * Template file for About.me block markup.
 */
?>
<div class="aboutme-display">
  <div class="about_me_image">
    <?php if ($aboutme_data['photo'] != NULL): ?>
      <?php print $aboutme_data['photo']; ?>
    <?php endif; ?>
  </div>
  <div class="about_me_display_name">
    <?php if ($aboutme_data['display_name'] != NULL): ?>
      <a href=<?php print $aboutme_data['profile_url']?>
         style="font-size: <?php print $aboutme_data['display_name_font_size'] ?>">
        <?php print $aboutme_data['display_name'] ?>
      </a>
    <?php endif; ?>
  </div>
  <h2 class="about_me_header">
    <?php if ($aboutme_data['header']): ?>
      <?php print $aboutme_data['header'] ?>
    <?php endif ?>
  </h2>
  <br/>

  <div class="about_me_bio">
    <?php if ($aboutme_data['bio'] != NULL): ?>
      <?php print $aboutme_data['bio']; ?>
    <?php endif; ?>
  </div>
  <div class="about_me_apps">
    <?php if ($aboutme_data['apps'] != NULL): ?>
      <?php foreach ($aboutme_data['apps'] as $key => $app): ?>
        <?php print l($app['icon'], $app['site_url'], array(
            'html' => TRUE,
            'attributes' => array('target' => '_blank'),
          )
        ) ?>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
</div>
