<?php

/**
 * @file
 * Settings form of the about me profile fetch.
 */

/**
 * About Me settings form.
 *
 * @return array
 *   The form structure.
 */
function aboutme_settings_form() {
  $form = array();

  $form['aboutme_block_title'] = array(
    '#title' => t('Block Title'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => variable_get('aboutme_block_title', ''),
  );

  $description = t('Add your About.me username. You can find the username in the URL after login to About.me account.')
      . '<br />' . t('For example, for the account url <a href="@link" target="_blank">https://about.me/drupal</a> the username is <strong> drupal </strong>.', array('@link' => url('https://about.me/drupal')));
  $form['aboutme_name'] = array(
    '#title' => t('Your about.me User Name'),
    '#type' => 'textfield',
    '#size' => 60,
    '#required' => TRUE,
    '#default_value' => variable_get('aboutme_name', ''),
    '#description' => $description,
  );

  $form['aboutme_photo_settings'] = array(
    '#title' => t('Photo'),
    '#type' => 'select',
    '#options' => array(
      'background_image' => t('Show Background Image'),
      'none' => t('None'),
    ),
    '#default_value' => variable_get('aboutme_photo_settings', ''),
  );

  $form['aboutme_name_settings'] = array(
    '#title' => t('Display Name'),
    '#type' => 'select',
    '#options' => array(
      '24px' => t("Display X-Large"),
      '19px' => t("Display Large"),
      '16px' => t("Display Medium"),
      '10px' => t("Display Small"),
      'dont_display' => t("Don't Display Name"),
    ),
    '#default_value' => variable_get('aboutme_name_settings', ''),
  );

  $form['aboutme_show_headline'] = array(
    '#title' => t('Headline'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('aboutme_show_headline', ''),
  );

  $form['aboutme_show_bio'] = array(
    '#title' => t('Bio'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('aboutme_show_bio', ''),
  );

  $form['aboutme_show_apps'] = array(
    '#title' => t('Apps'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('aboutme_show_apps', ''),
  );

  $form['#validate'][] = 'aboutme_username_validate';

  return system_settings_form($form);
}

/**
 * About me settings form validations.
 */
function aboutme_username_validate($form, &$form_state) {
  $aboutme_name = $form_state['values']['aboutme_name'];
  $query = array(
    'client_id' => ABOUTME_CLIENT_ID,
    'extended' => 'true',
    'on_match' => 'true',
    'strip_html' => 'false',
  );
  $options = array('query' => $query, 'https' => TRUE);
  $url = url('https://api.about.me/api/v2/json/user/view/' . $aboutme_name, $options);
  $options = array('method' => 'GET');
  $result = drupal_http_request($url, $options);
  if ($result->code == 200) {
    $data = drupal_json_decode($result->data);
  }
  if (isset($data['error_message']) || empty($data['user_name']) || $result->code == 400) {
    $link = l(t('What is About.me Username?'), "https://aboutme.zendesk.com/hc/en-us/articles/220407348-What-s-my-username-", array('html' => TRUE, 'attributes' => array('target' => '_blank')));
    form_set_error('aboutme_name', t('"@var" does not exists with About.me profiles, please enter a valid username/check your about.me profile for username.', array('@var' => $aboutme_name)));
    drupal_set_message(t($link), 'error');
  }
}
