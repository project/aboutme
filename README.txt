
-- SUMMARY --

The module integrates the about.me profile with Drupal and display the profile
information in a block which can be placed anywhere in the site.

-- REQUIREMENTS --

Block module (core)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70501 for further information.

-- CONFIGURATION --

Go to admin/config/services/aboutme and fill out the configuration form for the
block. Here you need to enter your about.me username, photo settings and other
settings which you want to pull from your about.me profile.

Currently only the basic informations are being fetched.

-- USAGE --

Once the configurations are saved a block will be created which can be placed
in any region or can be used in panels.

-- CONTRIBUTORS --

Maintainer
- Malabya Tewari <http://drupal.org/u/malavya>
